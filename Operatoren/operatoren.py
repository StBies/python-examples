#Rechenoperatoren
a = 5
b = 4

c = a + b
d = a - b
e = a * b
f = a / b #Cave: implizite float-conversion seit python3
g = a % b

#Zuweisung
a += 1
a -= 2
a *= 2
b /= 2

#Überprüfung
a = True
b = False

c = not a
xor = (a and not b) or (not a and b) #xor not (by default implemented)
d = a or b
e = a and b
f = a != b

#Bitweise operatoren
a = 5
b = 6

c = a & b #Bitweise und
print("a: {}\nb:{}\na&b: {}".format(bin(a),bin(b),bin(c)))

d = a | b #Bitweise oder
e = a ^ b #Bitweise xor
f = ~ a #Bitweise nicht
g = a << 1 #Bitweise rechts shiften
h = b >> 1 #Bitweise links shiften
