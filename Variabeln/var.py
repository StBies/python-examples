#Anlegen von Variablen ohne Typangabe. Muss direkt initialisiert werden.
var1 = 5
var2 = 1.4
var3 = "Beispiel"

#Variablen haben einen Typ!
print("Variablen haben Typen:")
print(type(var1),type(var2),type(var3))

#Typ ist aber nicht statisch
print("Variablen sind nicht statisch")
var1 = "was anderes"
print(type(var1))

#In der Praxis wird eine Variable bei jeder Zuweisung neu angelegt:
print("Variablen werden bei Zuweisung neu angelegt:")
print("Speicheradresse: {}".format(hex(id(var1))))
var1 = 1.5
print("Speicheradresse: {}".format(hex(id(var1))))
