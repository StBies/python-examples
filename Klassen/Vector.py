import math
class Vector:
    def __init__(self,x,y,z):
        self.x = x
        self.y = y
        self.z = z
		
    def length(self):
        x = self.x
        y = self.y
        z = self.z
        return math.sqrt(x**2 + y**2 + z**2)
		
    def vector_to(self,vector):
        x = vector.x - self.x
        y = vector.y - self.y
        z = vector.z - self.z
        return Vector(x, y, z)
