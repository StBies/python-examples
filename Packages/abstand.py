from Klassen import Vector #Klasse Vector aus Paket Klassen importieren
def abstand(vec1,vec2):
    r = vec1.vector_to(vec2)
    return r.length()

x = Vector(1,2,3)
y = Vector(0,1,5)
z = Vector(2,-1,4)

r1 = abstand(x,y)
r2 = abstand(x,z)
r3 = abstand(y,z)

print(r1,r2,r3)
