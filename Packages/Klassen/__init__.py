'''__init__.py muss in allen Ordnern sein, in denen .py Dateien sind, damit
diese vom Interpreter gefunden werden.

Dann von .py Datei in Verzeichnis oberhalb:
import Ordnername.importierte_sache
'''
#Aus der Datei Vector nur die Klasse Vector importieren
from Klassen.Vector import Vector #python3
#from Vector import Vector #python2
