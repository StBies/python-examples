import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import linregress
from scipy.optimize import curve_fit

'''
1.) Als "klassische" lineare Regression
'''
# 1.) Daten laden
x,y = np.loadtxt("lindata.dat", unpack=True)

# 2.) Fit durchfuehren
slope, intercept, r_value, p_value, std_err = linregress(x,y)

# 3.) Funktion zum Plotten erstellen
y_fit = slope * x + intercept

plt.plot(x,y,label="data")
plt.plot(x,y_fit,label="fit, m ={}, b={}".format(round(slope,3),round(intercept,3)))
plt.title("Lineare Regression")
plt.xlabel("x")
plt.ylabel("f(x)")
plt.legend()
plt.show()


'''
2.) Als curve_fit
'''

# 1.) Modellfunktion definieren

def linfunc(x,m,b):
    return m * x + b

# 2.) Fit durchfuehren
popt, pcov = curve_fit(linfunc, x, y)

# 3.) Funktion zum Plotten erstellen
y_fit = popt[0] * x + popt[1]

plt.plot(x,y,label="data")
plt.plot(x,y_fit,label="fit, m ={}, b={}".format(round(popt[0],3),round(popt[1],3)))
plt.title("curve_fit mit linearer Funktion")
plt.xlabel("x")
plt.ylabel("f(x)")
plt.legend()
plt.show()