import numpy as np

x = np.linspace(0, 1000, 200)
y = np.random.uniform() * x

noise = np.random.normal(0,10,len(x))

#y mit noise verschmieren

y += noise

with open("lindata.dat","w") as f:
    for pair in zip(x,y):
        f.write("{}\t{}\n".format(pair[0],pair[1]))