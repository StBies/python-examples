'''
Daten fitten mit scipy.optimize.curve_fit
'''


from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt

'''
1.) Daten lesen oder erzeugen.
    In diesem Fall: Daten aus ASCII-File lesen
'''
x_data, y_data = np.loadtxt("daten.dat", unpack=True)

'''
2.) Funktion definieren, die Modell beschreibt
    Hier: Normalverteilung
'''

#Erster Parameter muss Variable sein, danach Parameter des Fits
def gauss(x,a,mu,sigma):
    exponent = - (x-mu)**2 / (2 * sigma**2)
    return a * np.exp(exponent)

'''
3.) Fitten
'''

constraints = ([0,0,0.5],[np.inf,500,1]) #optional
#Parameter p0 sind Startwerte fuer fit
popt, pcov = curve_fit(gauss,x_data,y_data,bounds=constraints,p0=[200,2,0.5])
# popt sind optimierte Parameter
# pcov ist covariance matrix

'''
4.) Ergebnis plotten
'''
#Fitkurve erstellen
x_fit = np.linspace(-2,5,1000)
y_fit = [gauss(x,popt[0],popt[1],popt[2]) for x in x_fit]

plt.plot(x_data,y_data,label='Daten')
fitlabel = "fit:\na={}\n$\\mu$={}\n$\\sigma$={}".format(round(popt[0],3),round(popt[1],3),round(popt[2],3))
plt.plot(x_fit,y_fit,label=fitlabel)
plt.title("Daten mit Fit")
plt.xlabel("x")
plt.ylabel("y")
plt.legend()
plt.show()
