'''
Generiert Daten, die in fits.py gefittet werden
'''

import numpy as np
import matplotlib.pyplot as plt

data = np.random.normal(2,0.8,10000)
hist = plt.hist(data,bins=100)

x = hist[1][:-1]
y = hist[0]

#Normieren
#integral = sum(y)
#y /= integral

f = open("daten.dat","w")
for werte in zip(x,y):
    f.write("{}\t{}\n".format(werte[0],werte[1]))
f.close()
    
    
