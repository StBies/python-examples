#Eine Funktion definieren
#Beispiel: Addieren zweier Zahlen

def addieren(zahl1,zahl2):
    return zahl1 + zahl2

#Aufruf:
summe1 = addieren(1,2)

print("addieren(1,2) gibt zurück: {}".format(summe1))

#Das geht automatisch auch für Fließkomma:

summe2 = addieren(1.25, 1)
print("addieren(1.25, 1) gibt zurück: {}".format(summe2))


#Rekursion ist möglich
def factorial(n):
    if n < 0:
        return -1
    elif n <= 1:
        return 1
    else:
        return n * factorial(n - 1)

fac5 = factorial(5)
print("factorial(5) gibt zurück: {}".format(fac5))
