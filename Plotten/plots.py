'''
Erstellen von verschiedenen Plots in Python mit matplotlib
'''
#importieren der Bibliotheken
import matplotlib.pyplot as plt
import numpy as np # fuer schnelles Bauen von arrays mit Funktionswerten
import random

'''
1.) Funktionen f(x) plotten
1.1) Bsp: volle Sinus-Periode
'''
# 1.) Zwei arrays (x,y) erzeugen
min = 0
max = 2 * np.pi
x = np.arange(min,max,max / 1000) # aequidistante Werte (min,max,schrittweite) als array
y = np.sin(x) # sin-Funktion aus numpy kann array als argument nehmen und gibt array zurueck

# 2.) Plotten
plt.plot(x,y)
plt.title("Sinus-Funktion")
plt.xlabel("x")
plt.ylabel("f(x)")
plt.show()


# 3.) Plot verfeinern
# 3.1.) Beschriftung der x-Achse anpassen
xlabels = (np.arange(min,max,max / 8),
           ["0",
            "$\\frac{\\pi}{4}$",
            "$\\frac{\\pi}{2}$",
            "$\\frac{3\\pi}{4}$",
            "$\\pi$",
            "$\\frac{5\\pi}{4}$",
            "$\\frac{3\\pi}{2}$",
            "$\\frac{7\\pi}{4}$"]) #Latex geht, Backslash ist escape-char -> doppelter backslash!

plt.plot(x,y)
plt.xticks(xlabels[0],xlabels[1])
plt.title("$f(x) = \\sin(x)$")
plt.xlabel("$x$")
plt.ylabel("$f(x)$")
plt.grid()
plt.show()

# 3.2.) Mehrere Plots übereinander
y_cos = np.cos(x)

plt.plot(x,y,label="$f_{1}(x) = \\sin(x)$")
plt.plot(x,y_cos,label="$f_{2}(x) = \\cos(x)$")
plt.xticks(xlabels[0],xlabels[1])
plt.title("Sinus und Cosinus")
plt.xlabel("$x$")
plt.ylabel("$f(x)$")
plt.legend()
plt.grid()
plt.show()

# 4.) Subplots

fig = plt.figure()
axes1 = fig.add_subplot(211) # 211 = 2 Zeilen, 1 Spalte, Plot nr 1
axes2 = fig.add_subplot(212)

axes1.plot(x,y)
axes1.set_title("$f(x) = \\sin(x)$")
axes1.set_xlabel("x")
axes1.set_ylabel("f(x)")
axes1.set_xticks(xlabels[0])
axes1.set_xticklabels(xlabels[1])
axes2.plot(x,y_cos)
axes2.set_title("$f(x) = \\cos(x)$")
axes2.get_xaxis().set_label_text("x")
axes2.get_yaxis().set_label_text("f(x)")
axes2.set_xticks(xlabels[0])
axes2.set_xticklabels(xlabels[1])
plt.show()



'''
2.) Histogramme
2.1.) Bsp: Normalverteilung
'''
zufall = np.random.normal(0,200,10000) # (mean, dev, n_draws)
hist, bins, patches = plt.hist(zufall,bins=100) # Histogram
plt.title("Normalverteilung")
plt.xlabel("x")
plt.ylabel("N")
plt.show()

'''
2.2.) Binning
'''
plt.hist(zufall,bins=np.arange(-500,500,5)) #bins kann ein array sein
plt.title("Normalverteilung\nim Intervall $[-500,500]$ bei Binweite 5")
plt.xlabel("x")
plt.ylabel("N")
plt.show()


'''
3.) Scatter plots
3.1) Bsp: 2d Gauss-Profil
'''
#1.) Zufallszahlen erzeugen
x = np.random.normal(0,200,10000)
y = np.random.normal(0,200,10000)

plt.scatter(x, y, s=0.5) #Parameter s setzt die Punktgroesse
plt.title("Scatter plot")
plt.xlabel("x [a.u.]")
plt.ylabel("y [a.u.]")
plt.show()

'''
3.2.) Als 2d histogramm
'''
plt.hist2d(x, y, bins=40)
plt.title("2D Histogramm")
plt.xlabel("x [a.u.]")
plt.ylabel("y [a.u.]")
plt.colorbar()
plt.show()

'''
4.) Plot in Polarkoordinaten
Beispiele: https://matplotlib.org/gallery/pie_and_polar_charts/polar_scatter.html#sphx-glr-gallery-pie-and-polar-charts-polar-scatter-py
'''
N = 100
r = np.random.uniform(0,1,N)
phi = np.random.uniform(0, 2 * np.pi, N)

fig = plt.figure()
ax = fig.add_subplot(111, projection='polar')
ax.scatter(phi,r)
plt.show()

'''
5.) xkcd
Beispiele: https://matplotlib.org/gallery/showcase/xkcd.html#sphx-glr-gallery-showcase-xkcd-py
'''
    
with plt.xkcd(): #plt.xkcd nur fuer diesen plot
    x = np.arange(100)
    y = np.arange(100)
    y[50:] = np.arange(50,0,-1)
    plt.plot(x,y)
    plt.xticks([]) #keine xticks
    plt.yticks([]) #keine yticks
    plt.title("xkcd-style plot")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.show()

    
    
