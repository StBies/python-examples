#For-Schleifen:

for i in range(100): #i nimmt die Werte von 0 bis 99 an (100 Werte)
    print(i)

for i in range(0,100): #wie oben
    print(i)

for i in range(0,100,2): #geht in zweier Schritten von 0 bis 100
    print(i)
    
#Range jeweils exclusive der oberen Grenzen
    
#While-Schleifen

i = 0
while i < 100:
    i += 1
