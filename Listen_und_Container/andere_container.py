#Tuple
t1 = (1,2)
t2 = (1,2,3,4)
#Unterschied zur Liste: Elemente später nicht veränderbar, Tuple selbst schon
t2 = t2 + t1

#Über tuple iterieren:
for i in range(len(t2)):
    continue #do nothing

#Dictionary
dict1 = {} #Empty dictionary
dict2 = {'key1' : 2, 'key2' : 5}

#Neuen Eintrag in Dictionary einfügen:
dict2['key3'] = 4

#Über Dictionary iterieren
for key in dict2.keys():
    print("Key: {}, Eintrag: {}".format(key, dict2[key]))
