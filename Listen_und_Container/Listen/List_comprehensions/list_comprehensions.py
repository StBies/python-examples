import random

#Liste mit 100 Zufallszahlen von -10 bis 10
liste = [random.randint(-10,10) for i in range(100)]

#Daraus die letzten 3 abschneiden:
letzte_weg = liste[:-3]

#Die ersten 4 abschneiden:
erste_weg = liste[4:]

#Den ersten und letzten Eintrag abschneiden:
kurz = liste[1:-1]

#Neue Liste erzeugen, wo nur die ungeraden Werte enthalten sind:   
ungerade = [el for el in liste if (el % 2) == 1]

#Lister der Zahlen von 0 bis 99 erzeugen:
aufsteigend = [x for x in range(100)]
