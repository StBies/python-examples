#Leere Liste erzeugen
l1 = []

#Liste mit n Nullen erzeugen
n = 100
l2 = [0] * n #n * [0] geht auch

#An Liste ein Element anhängen
l2.append(1)

#Letztes Element löschen
l2.pop() #wird dabei return'ed -> var = l2.pop() geht

#Länge von Liste lesen:
size = len(l2)

#Über Liste iterieren:

#Mit For-Schleife
for i in range(len(l2)):
    l2[i] = i

#Mit "For-Each-Schleife"
for element in l2:
    print(element)
